<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Pessoa;


class PessoaController extends Controller
{
    public function show()
    {
        $pessoas = Pessoa::orderBy('created_at', 'desc')->paginate(10);
        return $pessoas;
    }
  
    public function create()
    {
        return view('pessoas.create');
    }
  
    public function store(Request $request)
    {
        $pessoa = new Pessoa;
        $pessoa->nome        = $request->nome;
        $pessoa->sobrenome        = $request->sobrenome;
        $pessoa->email        = $request->email;
        $pessoa->telefone        = $request->telefone;
        $pessoa->cpf        = $request->cpf;
        $pessoa->cnpj        = $request->cnpj;
        $pessoa->pessoaJuridica        = isset($request->pessoaJuridica) || $request->pessoaJuridica == true ? true : false;

        $pessoa->save();
        return $pessoa;
    }
   
    public function edit($id)
    {
        $pessoa = Pessoa::findOrFail($id);
        return $pessoa;
    }
  
    public function update(Request $request, $id)
    {
        $pessoa = Pessoa::findOrFail($id);
        $pessoa->nome        = $request->nome;
        $pessoa->sobrenome        = $request->sobrenome;
        $pessoa->telefone        = $request->telefone;
        $pessoa->email        = $request->email;
        $pessoa->cpf        = $request->cpf;
        $pessoa->cnpj        = $request->cnpj;
        $pessoa->pessoaJuridica        = isset($request->pessoaJuridica) || $request->pessoaJuridica == true ? true : false;
        $pessoa->save();
        return $pessoa;
    }
  
    public function destroy($id)
    {
        $pessoa = Pessoa::findOrFail($id);
        $pessoa->delete();
        return $id;
    }
}