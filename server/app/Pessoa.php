<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pessoa extends Model
{
    protected $fillable = ['nome','sobrenome','email','telefone', 'pessoaJuridica', 'cpf', 'cnpj'];
    protected $guarded = ['id', 'created_at', 'update_at'];
    protected $table = 'pessoas';
    static $lista = array();
}
