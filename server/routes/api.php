<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::get('/pessoa', 'PessoaController@show');
Route::post('/pessoa', 'PessoaController@store');
Route::post('/pessoa/delete/{id}', 'PessoaController@destroy');
Route::post('/pessoa/{id}', 'PessoaController@update');


