<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Pessoas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pessoas', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nome', 30);
            $table->string('sobrenome', 30);
            $table->boolean('pessoaJuridica');
            $table->text('email');
            $table->text('description')->nullable();
            $table->decimal('cnpj', 14)->nullable();
            $table->decimal('telefone', 14)->nullable();
            $table->decimal('cpf', 11)->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('pessoas');
    }
}
